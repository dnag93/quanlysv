class Student {
  constructor(maSV, tenSV, loaiSV, diemToan, diemLy, diemHoa, diemRenLuyen) {
    this.maSV = maSV;
    this.tenSV = tenSV;
    this.loaiSV = loaiSV;
    this.diemToan = +diemToan;
    this.diemLy = +diemLy;
    this.diemHoa = +diemHoa;
    this.diemRenLuyen = +diemRenLuyen;

    this.calcDiemTB = () => ((this.diemToan + this.diemLy + this.diemHoa) / 3).toFixed(2)
  }
}
