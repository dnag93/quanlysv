var studentList = [];

function addStudent() {
  const studentId = document.getElementById("txtMaSV").value;
  const studentName = document.getElementById("txtTenSV").value;
  const studentType = document.getElementById("loaiSV").value;
  const mathMark = document.getElementById("txtDiemToan").value;
  const physicsMark = document.getElementById("txtDiemLy").value;
  const chemistryMark = document.getElementById("txtDiemHoa").value;
  const trainingPoint = document.getElementById("txtDiemRenLuyen").value;

  const Student = {
    id: studentId,
    name: studentName,
    type: studentType,
    math: +mathMark,
    physics: +physicsMark,
    chemistry: +chemistryMark,
    training: +trainingPoint,
    calcAverage: function() {
      return (this.math + this.physics + this.chemistry) / 3;
    },
    classify: function() {
      const averageMark = this.calcAverage();
      if (averageMark >= 8) {
        return "Excellent";
      }
      if (averageMark >= 5) {
        return "Good";
      }
      return "Weak";
    }
  };

  studentList.push(Student);

  createTable();
}

//chuyển từ studentList => 14 dòng
/*
    <tr>
        <td>1</td>
        <td>hieu</td>
        <td>ngheo</td>
        <td>10</td>
        <td>100</td>
    </tr>
*/
function createTable() {
  var contentTable = "";
  for (var i = 0; i < studentList.length; i++) {
    const student = studentList[i];
    /* 
        lần 1 :student = studentList[0] => student.id = 1;
        lần 2 :student = studentList[1] => student.id = 2;
        lần 3 :student = studentList[2] => student.id = 3;
    */
    //string template
    contentTable += `
    <tr>
        <td>${student.id}</td>
        <td>${student.name}</td>
        <td>${student.type}</td>
        <td>${student.calcAverage()}</td>
        <td>${student.training}</td>
    </tr>`;
  }

  var num = 5;
  console.log(`Số: ${num}`);
  console.log(contentTable);
  document.getElementById("tbodySinhVien").innerHTML = contentTable;
}
