let addStudent = () => {
  const maSV = document.getElementById("txtMaSV").value;
  const tenSV = document.getElementById("txtTenSV").value;
  const loaiSV = document.getElementById("loaiSV").value;
  const diemToan = document.getElementById("txtDiemToan").value;
  const diemLy = document.getElementById("txtDiemLy").value;
  const diemHoa = document.getElementById("txtDiemHoa").value;
  const diemRenLuyen = document.getElementById("txtDiemRenLuyen").value;

  let isValid = checkInput(
    maSV,
    tenSV,
    diemToan,
    diemLy,
    diemHoa,
    diemRenLuyen
  );
  if (isValid) {
    const newStudent = new Student(
      maSV,
      tenSV,
      loaiSV,
      diemToan,
      diemLy,
      diemHoa,
      diemRenLuyen
    );
    studentList.push(newStudent);
    localStorage.setItem("listStudentSto", JSON.stringify(studentList));
    resetForm();
    renderTable(studentList);
  }
};

let renderTable = list => {
  let tableContent = "";
  list.forEach(student => {
    tableContent += `
    <tr>
        <th>${student.maSV}</th>
        <th>${student.tenSV}</th>
        <th>${student.loaiSV}</th>
        <th>${student.calcDiemTB()}</th>
        <th>${student.diemRenLuyen}</th>
        <td>
        <button
            class="btn btn-danger"
            onclick="removeStudent('${student.maSV}')"
        >
            Xoá
        </button>
        <button 
            class="btn btn-info"
            onclick="getUpdateStudent('${student.maSV}')"
        >
            Cập nhật
        </button>
        </td>

      </tr>
      `;
  });
  document.getElementById("tbodySinhVien").innerHTML = tableContent;
};

let getDataFromLocal = () => {
  const studentListStr = localStorage.getItem("listStudentSto");
  if (studentListStr) {
    studentListFromLocal = JSON.parse(studentListStr);
    studentListFromLocal.forEach(student => {
      const newStudent = new Student(
        student.maSV,
        student.tenSV,
        student.loaiSV,
        student.diemToan,
        student.diemLy,
        student.diemHoa,
        student.diemRenLuyen
      );
      studentList.push(newStudent);
    });
    renderTable(studentList);
  }
};
// =============================
let findStudentIndex = id =>
  studentList.findIndex(student => student.maSV === id);

let resetForm = () => document.getElementById("btnReset").click();

// =============================
let removeStudent = id => {
  const index = findStudentIndex(id);

  if (index >= 0) {
    studentList.splice(index, 1);
    localStorage.setItem("listStudentSto", JSON.stringify(studentList));
    renderTable(studentList);
  }
};

// =============================
let searchStudent = () => {
  const searchList = [];
  timer && clearTimeout(timer);
  timer = setTimeout(() => {
    const keyword = document
      .getElementById("txtSearch")
      .value.toLowerCase()
      .trim();

    studentList.forEach(student => {
      let lowerCaseName = student.tenSV.toLowerCase();
      (student.maSV === keyword || lowerCaseName.indexOf(keyword) !== -1) &&
        searchList.push(student);
    });
    renderTable(searchList);
  }, 400);
};

// =============================
let getUpdateStudent = id => {
  const index = findStudentIndex(id);
  if (index >= 0) {
    document.getElementById("txtMaSV").value = studentList[index].maSV;
    document.getElementById("txtTenSV").value = studentList[index].tenSV;
    document.getElementById("loaiSV").value = studentList[index].loaiSV;
    document.getElementById("txtDiemToan").value = studentList[index].diemToan;
    document.getElementById("txtDiemLy").value = studentList[index].diemLy;
    document.getElementById("txtDiemHoa").value = studentList[index].diemHoa;
    document.getElementById("txtDiemRenLuyen").value =
      studentList[index].diemRenLuyen;

    document.getElementById("btnUpdate").style.display = "";
    document.getElementById("btnAdd").style.display = "none";
    document.getElementById("txtMaSV").setAttribute("readonly", true);
  }
};

let updateStudent = () => {
  const maSV = document.getElementById("txtMaSV").value;
  const tenSV = document.getElementById("txtTenSV").value;
  const loaiSV = document.getElementById("loaiSV").value;
  const diemToan = document.getElementById("txtDiemToan").value;
  const diemLy = document.getElementById("txtDiemLy").value;
  const diemHoa = document.getElementById("txtDiemHoa").value;
  const diemRenLuyen = document.getElementById("txtDiemRenLuyen").value;

  let isValid = checkInput(
    maSV,
    tenSV,
    diemToan,
    diemLy,
    diemHoa,
    diemRenLuyen
  );
  if (isValid) {
    const updateStudent = new Student(
      maSV,
      tenSV,
      loaiSV,
      diemToan,
      diemLy,
      diemHoa,
      diemRenLuyen
    );

    const index = findStudentIndex(updateStudent.maSV);
    studentList[index] = updateStudent;
    localStorage.setItem("listStudentSto", JSON.stringify(studentList));

    document.getElementById("btnUpdate").style.display = "none";
    document.getElementById("btnAdd").style.display = "";
    document.getElementById("txtMaSV").removeAttribute("readonly");

    resetForm();
    renderTable(studentList);
  }
};
// =============================
let checkEmpty = (value, idSpan) => {
  const spanMessage = document.getElementById(idSpan);

  if (!value) {
    spanMessage.innerHTML = "(*) Dữ liệu bắt buộc nhập!";
    return false;
  }
  spanMessage.innerHTML = "";
  return true;
};

let checkLength = (value, idSpan, min, max) => {
  const spanMessage = document.getElementById(idSpan);
  if (value.length >= min && value.length <= max) {
    spanMessage.innerHTML = "";
    return true;
  }
  spanMessage.innerHTML = `(*) Dữ liệu nhập từ ${min} đến ${max} kí tự`;
  return false;
};

let checkNameVN = (value, idSpan) => {
  const pattern = new RegExp(
    "^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶ" +
      "ẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợ" +
      "ụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s]+$"
  );
  const spanMessage = document.getElementById(idSpan);
  if (pattern.test(value)) {
    spanMessage.innerHTML = "";
    return true;
  }
  spanMessage.innerHTML = `(*) Dữ liệu nhập phải là chữ.`;
  return false;
};

let checkId = (value, idSpan) => {
  const pattern = new RegExp("^[a-zA-Z0-9]+$");
  const spanMessage = document.getElementById(idSpan);
  if (pattern.test(value)) {
    spanMessage.innerHTML = "";
    return true;
  }
  spanMessage.innerHTML = `(*) Dữ liệu nhập phải là kí tự không dấu và số.`;
  return false;
};

let checkTrainingGrade = (value, idSpan) => {
  const spanMessage = document.getElementById(idSpan);
  const pattern = new RegExp("^[0-9]+$");
  if (!pattern.test(value)) {
    spanMessage.innerHTML = `(*) Điểm rèn luyện là số nguyên.`;
    return false;
  } else if (value < 0 || value > 100) {
    spanMessage.innerHTML = `(*) Điểm rèn luyện từ 0 tới 100.`;
    return false;
  }

  spanMessage.innerHTML = "";
  return true;
};

let checkSubjectGrade = (value, idSpan) => {
  const spanMessage = document.getElementById(idSpan);
  if (value >= 0 && value <= 10) {
    spanMessage.innerHTML = "";
    return true;
  }
  spanMessage.innerHTML = `Điểm môn học từ 0 tới 10.`;
  return false;
};

// =============================
let checkInput = (maSV, tenSV, diemToan, diemLy, diemHoa, diemRenLuyen) => {
  let isValid = true;
  isValid &=
    checkEmpty(maSV, "idError") &&
    checkLength(maSV, "idError", 5, 8) &&
    checkId(maSV, "idError");

  isValid &= checkEmpty(tenSV, "nameError") && checkNameVN(tenSV, "nameError");

  isValid &=
    checkEmpty(diemToan, "mathGradeError") &&
    checkSubjectGrade(diemToan, "mathGradeError");

  isValid &=
    checkEmpty(diemLy, "physicGradeError") &&
    checkSubjectGrade(diemLy, "physicGradeError");

  isValid &=
    checkEmpty(diemHoa, "chemistryGradeError") &&
    checkSubjectGrade(diemHoa, "chemistryGradeError");

  isValid &=
    checkEmpty(diemRenLuyen, "trainingGradeError") &&
    checkTrainingGrade(diemRenLuyen, "trainingGradeError");

  return isValid;
};
// =============================
let studentList = [];
let timer;
getDataFromLocal();

// * Chức năng nâng cao(làm thêm) :
// phân trang cho danh sách sinh viên,
// mỗi trang chỉ hiện 5 sinh viên


// validate quá nhiều TH đặc biệt
// testcase
// "   " >> dính nhiều nhát
// -
// 0 > bị tính là false - bắt buộc nhập
//  "  3"
// "3   "
// ".0"
// "test"
// "3test"
// 0test
// value.length sẽ có TH undefined
// hoặc phải dùng switch case / if hardcore
